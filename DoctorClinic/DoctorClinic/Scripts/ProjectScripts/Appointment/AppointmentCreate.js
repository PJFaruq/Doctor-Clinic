﻿$(document).ready(function () {

    $(".datePicker").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-0:2+"

    });

    $("body").on("click", "#SerialNum p", function () {

        var serial = $(this).text();
        $("#SerialNo").val(serial).css("style","margin-top:5px");

    });

    //function test() {

    //    var DoctorId = (this).val();
    //    $.ajax({
    //        url: "/Appoinments/GetSchedule",
    //        type: "post",
    //        data: { DoctorId: DoctorId },
    //        success: function (response) {
    //            if (response.row == 0) {
    //                $("#SchedulList").html("No Schedule Available").css("background-color", "red");
    //            }
    //        }
    //    });
    //}
    //$("#DoctorId").change(function () {

    //});
});



//Patient Checking

$("#PatientEmail").on("keyup", function () {
    var patientEmail = $(this).val();
    if (patientEmail == "") {
        $("#PatientName").empty();
    }

    if (patientEmail != "") {
        $.ajax({

            url: "/Appointments/CheckPatient",
            type: "post",
            data: { patientEmail: patientEmail },
            success: function (response) {
                if (response != null) {
                    $("#PatientName").text(response.FirstName + " " + response.LastName).css({ "color": "green", "font-weight": "bold" });
                }
                if (response == 0) {
                    $("#PatientName").text("This patient is not Exist").css({ "color": "red", });

                }
            }


        })
    }



});


//Get Doctor By Department
$("#DepartmentId").on("change", function () {
    var DeptId = $(this).val();

    $.ajax({
        url: "/Appointments/GetDoctorList",
        type: "post",
        data: { DepartmentId: DeptId },

        success: function (response) {

            $("#DoctorId").empty();
            $("#DoctorId").append("<option value=''>---Select Doctor---</option>");
            $.each(response, function (index, row) {

                $("#DoctorId").append("<option value='" + row.Id + "'>" + row.FirstName + " " + row.LastName + "</option>")

            });

        }
    });
});


//Check Serial By Date
$("#Date").on("change", function () {
    var date = $(this).val();
    var doctorId = $("#DoctorId").val();

    $.ajax({
        url: "/Appointments/CheckSerial",
        type: "post",
        data: { date: date, doctorId: doctorId },
        success: function (response) {
            $("#SerialNum").empty();
            if (response == 0) {
                $("#SerialNum").html("No Serial Avaiable").css({ "color": "red", "font-size": "20px" });
            }

            if (response != null && response !=0) {

                $.each(response, function (index, value) {

                    //$("#SerialNum").append("<p value='" + value + "' class='btn btn-primary' id='SerialLabel'>" + value + "</p>");
                    $("#SerialNum").append("<p class='btn btn-primary'>" + value + "</p>");
                });
            }


        }
    });
});