﻿$(document).ready(function () {
    $("#Image").change(function () {
        $("#CurrentImage").hide();
    });


    var division = $("#CurrentDivision").val();
    var CurrentDistrict = $("#CurrentDistrict").val();
    var CurrentSubDistrict = $("#CurrentSubDistrict").val();
    var CurrentHospital = $("#CurrentHospital").val();
    $.ajax({
        url: "/Doctors/GetAllDistrictById",
        type: "post",
        data: { id: division },
        success: function (response) {
            $("#DistrictId").empty();
            $("#DistrictId").append("<option value=''>" + "--Select District--" + "</option>");
            $.each(response, function (index, value) {
                $("#DistrictId").append("<option value='" + value.Id + "'>" + value.Name + "</option>");
            });
            $("#DistrictId").val(CurrentDistrict);
        }
    });

    
    $.ajax({
        url: "/Doctors/GetAllSubDistrict",
        type: "post",
        data: { id: CurrentDistrict },
        success: function (response) {
            $("#SubDistrictId").empty();
            $("#SubDistrictId").append("<option value=''>" + "Select SubDistrict" + "</option>");
            $.each(response, function (index, value) {
                $("#SubDistrictId").append("<option value='" + value.Id + "'>" + value.Name + "</option>");
            });
            $("#SubDistrictId").val(CurrentSubDistrict);
        }
    });

    
    $.ajax({
        url: "/Doctors/GetAllHospitalById",
        type: "post",
        data: { id: CurrentSubDistrict },
        success: function (response) {
            $("#HospitalId").empty();
            $("#HospitalId").append("<option value=''>" + "Select Hospital" + "</option>");
            $.each(response, function (index, value) {
                $("#HospitalId").append("<option value='" + value.Id + "'>" + value.Name + "</option>");
            });
            $("#HospitalId").val(CurrentHospital);
        }
    });

    //Dropdown for Region
    //Get All District for DropDown
    $("#DivisionId").change(function () {
        var divisionId = $(this).val();
        $.ajax({
            url: "/Doctors/GetAllDistrictById",
            type: "post",
            data: { id: divisionId },
            success: function (response) {
                $("#DistrictId").empty();
                $("#DistrictId").append("<option value=''>" + "--Select District--" + "</option>");
                $.each(response, function (index, value) {
                    $("#DistrictId").append("<option value='" + value.Id + "'>" + value.Name + "</option>");
                });
            }
        });

    });

    //Get All Sub District for DropDown
    $("#DistrictId").on("change", function () {

        var districtId = $(this).val();
        $.ajax({
            url: "/Doctors/GetAllSubDistrict",
            type: "post",
            data: { id: districtId },
            success: function (response) {
                $("#SubDistrictId").empty();
                $("#SubDistrictId").append("<option value=''>" + "Select SubDistrict" + "</option>");
                $.each(response, function (index, value) {
                    $("#SubDistrictId").append("<option value='" + value.Id + "'>" + value.Name + "</option>");
                });
            }
        });
    });

    //Get All Hospital for DropDown
    $("#SubDistrictId").on("change", function () {

        var SubDistrictId = $(this).val();
        $.ajax({
            url: "/Doctors/GetAllHospitalById",
            type: "post",
            data: { id: SubDistrictId },
            success: function (response) {
                $("#HospitalId").empty();
                $("#HospitalId").append("<option value=''>" + "Select Hospital" + "</option>");
                $.each(response, function (index, value) {
                    $("#HospitalId").append("<option value='" + value.Id + "'>" + value.Name + "</option>");
                });
            }
        });
    });



    //Check Doctor Mail
    $("#Email").blur(function () {
        CheckEmail();
    });
    function CheckEmail() {
        var email = $("#Email").val();
        var type = $("input[name='Type']:checked").val();
        var CurrentEmail = $("#CurrentEmail").val();

        if (email != "" ) {
            $.ajax({
                url: "/Doctors/CheckDoctorEmail",
                data: { email: email, type: type },
                type: "post",
                success: function (response) {
                    if (response == 1) {
                        if (email == CurrentEmail && type == CurrentType) {
                            isAvaiable = false;
                            $("#ErrorMsg").empty();
                        }
                        else {
                            if (type == "Customer") {
                                $("#ErrorMsg").text("This Email is Already Exist as a Customer");
                            }
                            if (type == "Supplier") {
                                $("#ErrorMsg").text("This Email is Already Exist as a Supplier");
                            }
                            isAvaiable = true;
                        }

                    }
                    else {
                        $("#ErrorMsg").empty();
                        isAvaiable = false;
                    }
                }

            })
        }
    }
});