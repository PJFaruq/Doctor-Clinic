﻿var GetDeleteId = function (Id) {
    $("#DeleteId").val(Id);
    $("#myModal").modal("show");
}

$("#btnConfirmDelete").on("click", function () {
    var id = $("#DeleteId").val();
    $.ajax({
        url: "/Doctors/Delete",
        method: "post",
        data: { id: id },
        success: function (response) {
            if (response == 1) {
                $("#DelRow_" + id).remove();
                $("#myModal").modal("hide");
            }
        }
    })
});