﻿$(document).ready(function () {
    $("#DateOfBirth").datepicker();

    //Get All District for DropDown
    $("#DivisionId").change(function () {
        var divisionId = $(this).val();
        $.ajax({
            url: "/Doctors/GetAllDistrictById",
            type: "post",
            data: { id: divisionId },
            success: function (response) {
                $("#DistrictId").empty();
                $("#DistrictId").append("<option value=''>"+"--Select District--"+"</option>");
                $.each(response, function (index, value) {
                    $("#DistrictId").append("<option value='" + value.Id + "'>" + value.Name + "</option>");
                });
            }
        });

    });

    //Get All Sub District for DropDown
    $("#DistrictId").on("change", function () {

        var districtId = $(this).val();
        $.ajax({
            url: "/Doctors/GetAllSubDistrict",
            type: "post",
            data: { id: districtId },
            success: function (response) {
                $("#SubDistrictId").empty();
                $("#SubDistrictId").append("<option value=''>" + "Select SubDistrict" + "</option>");
                $.each(response, function (index, value) {
                    $("#SubDistrictId").append("<option value='" + value.Id + "'>" + value.Name + "</option>");
                });
            }
        });
    });

    //Get All Hospital for DropDown
    $("#SubDistrictId").on("change", function () {

        var HospitalId = $(this).val();
        $.ajax({
            url: "/Doctors/GetAllHospitalById",
            type: "post",
            data: { id: HospitalId },
            success: function (response) {
                $("#HospitalId").empty();
                $("#HospitalId").append("<option value=''>" + "Select Hospital" + "</option>");
                $.each(response, function (index, value) {
                    $("#HospitalId").append("<option value='" + value.Id + "'>" + value.Name + "</option>");
                });
            }
        });
    });

})







