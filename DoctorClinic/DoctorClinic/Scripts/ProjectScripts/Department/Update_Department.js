﻿
$(document).ready(function () {

    var isAvailable = false;

    $("#form").submit(function () {
        if (isAvailable == true) {
            return false;
        }
    });


    $("#Name").blur(function () {
        var name = $("#Name").val();
        var currentName = $("#CurrentName").val();
        $.ajax({
            url: "/Departments/CheckDepartmentName",
            method: "post",
            data: { name: name },
            success: function (response) {
                if (response == 1) {
                    if (name == currentName) {
                        isAvailable = false;
                        $("#ErrorMsgForName").empty();

                    }
                    else {
                        $("#ErrorMsgForName").html("This Name is Already Exist");
                        isAvailable = true;
                    }


                }
                else {
                    $("#ErrorMsgForName").empty();
                    isAvailable = false;
                }
            }
        });
    });
});