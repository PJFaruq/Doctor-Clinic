﻿using AppointmentClinic.BLL;
using DoctorClinic.BLL;
using DoctorClinic.Models;
using DoctorClinic.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppointmentClinic.Controllers
{
    public class AppointmentsController : Controller
    {
        bool status = false;
        CommonManager commonManager = new CommonManager();
        AppointmentManager appointmentManager;
        public AppointmentsController()
        {
            appointmentManager = new AppointmentManager();
        }

        //GET: Create Appointment
        public ActionResult Create()
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            return View();
        }


        //POST: Create Appointment
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Appointment appointment,bool? IsFromPatientProfile)
        {
            if (appointment.SerialNo == 0)
            {
                ModelState.AddModelError("SerialNo", "Please Select a Serial No.");
                return View(appointment);
            }
            //if (!db.Patients.Any(model => model.Email == appointment.PatientId))
            //{
            //    ModelState.AddModelError("PatientId", "This Patient is already Exist.Try Another");
            //}

            if (ModelState.IsValid)
            {
                
                status = appointmentManager.Add(appointment);
                if (status)
                {
                    if (IsFromPatientProfile==true)
                    {
                        return RedirectToAction("Appointments", "Patients");
                    }

                    @ViewBag.Msg = "Appointment Added Successfully";
                    ModelState.Clear();
                    ViewBag.DepartmentId = commonManager.GetAllDepartment();
                    return View();

                }
                else
                {
                    @ViewBag.Msg = "Appointment Added Failed !";
                }
            }

            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            return View(appointment);
        }


        //GET: Update Appointment
        public ActionResult Update(int? id)
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }

            Appointment appointment = appointmentManager.GetById(id);
            if (appointment == null)
            {
                return RedirectToAction("Error");
            }
            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            ViewBag.DivisionId = commonManager.GetAllDivision();
            return View(appointment);
        }


        //POST: Update Appointment
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(Appointment appointment, HttpPostedFileBase ImageFile, byte[] CurrentImage)
        {
            

            if (ModelState.IsValid)
            {

                status = appointmentManager.Update(appointment);
                if (status == true)
                {
                    return RedirectToAction("ShowAll");
                }
                if (status == false)
                {

                    ViewBag.Msg = "Party Category Added Failed !";
                }

            }

            return View(appointment);
        }

        //Show All Appointment
        public ActionResult ShowAll()
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            return View(appointmentManager.GetAll());
        }


        //Detail of a Appointment
        public ActionResult Details(int? id)
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Appointment appointment = appointmentManager.GetById(id);
            if (appointment == null)
            {
                return RedirectToAction("Error", "Home");
            }

            Patient patient = appointmentManager.GetPatientByEmail(appointment.PatientEmail);
            Scheduler schedule = appointmentManager.GetSchedule(appointment.Date, appointment.DoctorId);
            //Patient patient = db.Patients.SingleOrDefault(model => model.Email == appointment.PatientId);
            //Scheduler schedule = db.Schedulers.SingleOrDefault(model => model.AvailableDay == appointment.AppoinmentDate.DayOfWeek.ToString() && model.DoctorId == appointment.DoctorId);

            AppointmentDetailVm appoinDetail = new AppointmentDetailVm();

            appoinDetail.AppointmentId = appointment.Id;
            appoinDetail.DoctorName = appointment.Doctor.FirstName + " " + appointment.Doctor.LastName;
            appoinDetail.Department = appointment.Department.Name;
            appoinDetail.Day = appointment.Date.DayOfWeek.ToString();
            appoinDetail.ScheduleTime = schedule.StartTime + "-" + schedule.EndTime;
            appoinDetail.AppointmentDate = appointment.Date;
            appoinDetail.Image = patient.Image;
            appoinDetail.FullName = patient.FirstName + " " + patient.LastName;
            appoinDetail.DateOfBirth = patient.DateOfBirth.ToString();
            appoinDetail.Age = Convert.ToString((DateTime.Now.Year - Convert.ToDateTime(patient.DateOfBirth).Year));
            appoinDetail.Gender = patient.Gender;
            appoinDetail.Mobile = patient.Mobile;
            appoinDetail.Problem = appointment.Problem;
            appoinDetail.SerialNo = "#0" + appointment.SerialNo.ToString();
            return View(appoinDetail);
        }

        //Delete a Appointment
        public JsonResult Delete(int? id)
        {
            status = appointmentManager.Delete(id);
            if (status)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }



        public JsonResult CheckPatient(string patientEmail)
        {
            status = appointmentManager.CheckPatient(patientEmail);

            if (status)
            {

                Patient patient =appointmentManager.GetPatientByEmail(patientEmail);
                return Json(patient, JsonRequestBehavior.AllowGet);
            }
                return Json(0);

        }

        public JsonResult GetDoctorList(int departmentId)
        {
            List<Doctor> doctorList = appointmentManager.GetDoctorListByDepartmentId(departmentId);
            return Json(doctorList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckSerial(DateTime date, int doctorId)
        {
            List<int> SerialList =appointmentManager.CheckSerial(date, doctorId);
            if (SerialList.Count>0)
            {
                return Json(SerialList, JsonRequestBehavior.AllowGet);

            }
                return Json(0);


        }


    }
}