﻿using DoctorClinic.Models;
using DoctorClinic.Models.DatabaseContext;
using DoctorClinic.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoctorClinic.Controllers
{
    public class AuthorizationsController : Controller
    {
        private DoctorClinicDbContext db = new DoctorClinicDbContext();

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Login login)
        {

            if (db.Doctors.Any(model => model.Email == login.Email && model.Password == login.Password))
            {
                int id = db.Doctors.Single(model => model.Email == login.Email && model.Password == login.Password).Id;
                Session["Id"] = id;
                Session["Role"] = "Doctor";
                return RedirectToAction("Index", "Home");
            }
            else if (db.Patients.Any(model => model.Email == login.Email && model.Password == login.Password))
            {
                Patient patient = db.Patients.Single(model => model.Email == login.Email && model.Password == login.Password);
                Session["Id"] = patient.Id;
                Session["PatientEmail"] = patient.Email;
                Session["Role"] = "Patient";
                return RedirectToAction("Index", "Home");
            }
            else if (login.Email == "admin@gmail.com" && login.Password == "1234")
            {
                
                Session["Id"] = "1";
                Session["AdminEmail"] = "Admin@gmail.com";
                Session["Role"] = "Admin";
                return RedirectToAction("AdminDashboard", "Home");
            }
            else
                ViewBag.ErrorMsg = "Incorrect Username or Password";
            return View();

        }

        public ActionResult LogOut()
        {
            Session["Id"] = null;
            Session["Role"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}