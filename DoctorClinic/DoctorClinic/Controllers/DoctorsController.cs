﻿using AppointmentClinic.BLL;
using DoctorClinic.BLL;
using DoctorClinic.Models;
using DoctorClinic.Models.Region;
using DoctorClinic.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoctorClinic.Controllers
{
    public class DoctorsController : Controller
    {
        bool status = false;
        CommonManager commonManager = new CommonManager();
        DoctorManager doctorManager;
        public DoctorsController()
        {
            doctorManager = new DoctorManager();
        }

        //GET: Create Doctor
        public ActionResult Create()
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            ViewBag.DivisionId = commonManager.GetAllDivision();
            return View();
        }


        //POST: Create Doctor
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Doctor doctor,HttpPostedFileBase ImageFile)
        {

            if (ImageFile == null)
            {
                ModelState.AddModelError("Image", "Please Upload an Image");
                
            }

            if (ImageFile != null)
            {
                bool isValidFormat = commonManager.CheckImageFormat(ImageFile);
                if (isValidFormat == false)
                {
                    ModelState.AddModelError("Image", "Only jpg , png , jpeg are allowed");
                }
                else
                {
                    byte[] convertedImage = commonManager.ConvertImage(ImageFile);
                    doctor.Image = convertedImage;
                }


            }

            if (ModelState.IsValid)
            {
                bool isExistEmail = doctorManager.CheckDoctorEmail(doctor.Email);
                if (isExistEmail)
                {
                    ModelState.AddModelError("Name", "This Name is Already Exist");
                    return View();
                }
                status = doctorManager.Add(doctor);
                if (status)
                {

                    @ViewBag.Msg = "Doctor Added Successfully";
                    ModelState.Clear();

                }
                else
                {
                    @ViewBag.Msg = "Doctor Added Failed !";
                }
            }
            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            ViewBag.DivisionId = commonManager.GetAllDivision();
            return View();
        }


        //GET: Update Doctor
        public ActionResult Update(int? id)
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }

            Doctor doctor = doctorManager.GetById(id);
            if (doctor == null)
            {
                return RedirectToAction("Error");
            }
            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            ViewBag.DivisionId = commonManager.GetAllDivision();
            return View(doctor);
        }


        //POST: Update Doctor
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(Doctor doctor, HttpPostedFileBase ImageFile, byte[] CurrentImage,bool? IsFromDoctorProfile)
        {
            if (ImageFile == null)
            {

                doctor.Image = CurrentImage;
            }

            if (ImageFile != null)
            {
                bool isValidFormat = commonManager.CheckImageFormat(ImageFile);
                if (isValidFormat == false)
                {
                    doctor.Image = CurrentImage;
                    ModelState.AddModelError("Image", "Only jpg , png , jpeg are allowed");
                }
                else
                {
                    doctor.Image = commonManager.ConvertImage(ImageFile);
                }

            }


            if (ModelState.IsValid)
            {

                status = doctorManager.Update(doctor);
                if (status == true)
                {
                    if (IsFromDoctorProfile==true)
                    {
                        return RedirectToAction("ProfileDetail", new { doctor.Id });
                    }
                    return RedirectToAction("ShowAll");
                }
                if (status == false)
                {

                    ViewBag.Msg = "Party Category Added Failed !";
                }

            }

            return View(doctor);
        }

        //Show All Doctor
        public ActionResult ShowAll()
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            return View(doctorManager.GetAll());
        }


        //Detail of a Doctor
        public ActionResult Details(int? id)
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }

            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Doctor doctor = doctorManager.GetById(id);
            if (doctor == null)
            {
                return RedirectToAction("Error", "Home");
            }


            return View(doctor);
        }

        //Delete a Doctor
        public JsonResult Delete(int? id)
        {
            
            status = doctorManager.Delete(id);
            if (status)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }

        //Check Doctor Email Availability
        public JsonResult CheckDoctorEmail(string Email, string CurrentEmail)
        {

            if (Email == CurrentEmail)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            status = doctorManager.CheckDoctorEmail(Email);
            if (status)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

        }

        //Get All District
        public JsonResult GetAllDistrictById(int id)
        {
            
            List<District> listOfDistrict = commonManager.GetAllDistrictById(id);
            return Json(listOfDistrict, JsonRequestBehavior.AllowGet);
        }

        //Get All Sub-District
        public JsonResult GetAllSubDistrict(int id)
        {
            List<SubDistrict> listOfSubDistrict = commonManager.GetAllSubDistrictById(id);
            return Json(listOfSubDistrict, JsonRequestBehavior.AllowGet);
        }


        //Get All Hospital
        public JsonResult GetAllHospitalById(int id)
        {
            List<Hospital> listOfHospital = commonManager.GetAllHospitalById(id);
            var filterListOfHospital = listOfHospital.Select(c => new {Id= c.Id,Name= c.Name });

            return Json(filterListOfHospital, JsonRequestBehavior.AllowGet);
        }

        //Profile Information

        public ActionResult ProfileDetail(int? id)
        {
            if ((string)Session["Role"] != "Doctor")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Doctor doctor = doctorManager.GetById(id);
            if (doctor == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View(doctor);
        }


        //Edit Doctor Profile
        public ActionResult EditProfile(int? id)
        {
            if ((string)Session["Role"] != "Doctor")
            {
                return RedirectToAction("Index", "Home");
            }

            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Doctor doctor = doctorManager.GetById(id);
            if (doctor == null)
            {
                return RedirectToAction("Error", "Home");
            }

            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            ViewBag.DivisionId = commonManager.GetAllDivision();
            return View(doctor);
        }


        //Get Doctor Appointment
        public ActionResult Appointments()
        {

            if ((string)Session["Role"] != "Doctor")
            {
                return RedirectToAction("Index", "Home");
            }
            int id =Convert.ToInt32(Session["Id"]);
            List<Appointment> appointments = doctorManager.GetAppointmentByDoctorId(id);

            return View(appointments);
        }


        //Details of Patients Appointment
        public ActionResult AppointmentDetails(int? id)
        {
            if ((string)Session["Role"] != "Doctor")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            AppointmentManager appointmentManager = new AppointmentManager();
            Appointment appointment = appointmentManager.GetById(id);
            if (appointment == null)
            {
                return RedirectToAction("Error", "Home");
            }

            Patient patient = appointmentManager.GetPatientByEmail(appointment.PatientEmail);
            Scheduler schedule = appointmentManager.GetSchedule(appointment.Date, appointment.DoctorId);
            //Patient patient = db.Patients.SingleOrDefault(model => model.Email == appointment.PatientId);
            //Scheduler schedule = db.Schedulers.SingleOrDefault(model => model.AvailableDay == appointment.AppoinmentDate.DayOfWeek.ToString() && model.DoctorId == appointment.DoctorId);

            AppointmentDetailVm appoinDetail = new AppointmentDetailVm();

            appoinDetail.AppointmentId = appointment.Id;
            appoinDetail.DoctorName = appointment.Doctor.FirstName + " " + appointment.Doctor.LastName;
            appoinDetail.Department = appointment.Department.Name;
            appoinDetail.Day = appointment.Date.DayOfWeek.ToString();
            appoinDetail.ScheduleTime = schedule.StartTime + "-" + schedule.EndTime;
            appoinDetail.AppointmentDate = appointment.Date;
            appoinDetail.Image = patient.Image;
            appoinDetail.FullName = patient.FirstName + " " + patient.LastName;
            appoinDetail.DateOfBirth = patient.DateOfBirth.ToString();
            appoinDetail.Age = Convert.ToString((DateTime.Now.Year - Convert.ToDateTime(patient.DateOfBirth).Year));
            appoinDetail.Gender = patient.Gender;
            appoinDetail.Mobile = patient.Mobile;
            appoinDetail.Problem = appointment.Problem;
            appoinDetail.SerialNo = "#0" + appointment.SerialNo.ToString();
            return View(appoinDetail);
        }


    }
}