﻿using DoctorClinic.BLL;
using DoctorClinic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoctorClinic.Controllers
{
    public class SchedulersController : Controller
    {
        bool status = false;
        CommonManager commonManager = new CommonManager();
        SchedulerManager schedulerManager;
        public SchedulersController()
        {
            
            schedulerManager = new SchedulerManager();
        }

        //GET: Create Scheduler
        public ActionResult Create()
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.DoctorId = commonManager.GetSelectListOfDoctor();
            return View();
        }


        //POST: Create Scheduler
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Scheduler scheduler)
        {

            if (ModelState.IsValid)
            {
                
                status = schedulerManager.Add(scheduler);
                if (status)
                {

                    @ViewBag.Msg = "Scheduler Added Successfully";
                    ModelState.Clear();

                }
                else
                {
                    @ViewBag.Msg = "Scheduler Added Failed !";
                }
            }
            ViewBag.DoctorId = commonManager.GetSelectListOfDoctor();
            return View();
        }


        //GET: Update Scheduler
        public ActionResult Update(int? id)
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }

            Scheduler scheduler = schedulerManager.GetById(id);
            if (scheduler == null)
            {
                return RedirectToAction("Error");
            }
            ViewBag.DoctorId = commonManager.GetSelectListOfDoctorById(scheduler.DoctorId);
            return View(scheduler);
        }


        //POST: Update Scheduler
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(Scheduler scheduler)
        {
            if (ModelState.IsValid)
            {

                status = schedulerManager.Update(scheduler);
                if (status == true)
                {
                    return RedirectToAction("ShowAll");
                }
                if (status == false)
                {

                    ViewBag.Msg = "Schedule Added Failed !";
                }

            }

            return View(scheduler);
        }

        //Show All Scheduler
        public ActionResult ShowAll()
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            return View(schedulerManager.GetAll());
        }


        //Detail of a Scheduler
        public ActionResult Details(int? id)
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Scheduler scheduler = schedulerManager.GetById(id);
            if (scheduler == null)
            {
                return RedirectToAction("Error", "Home");
            }


            return View(scheduler);
        }

        //Delete a Scheduler
        public JsonResult Delete(int? id)
        {
            status = schedulerManager.Delete(id);
            if (status)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }

        //Check Existing  Scheduling time 
        public JsonResult CheckDoctorIdAndDay(string AvailableDay, int DoctorId, string EditAvailableDay, int? EditDoctorId)
        {

            if (DoctorId == EditDoctorId && AvailableDay == EditAvailableDay)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            bool isAvailable = schedulerManager.CheckExistingSchedule(AvailableDay, DoctorId);
            if (isAvailable)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}