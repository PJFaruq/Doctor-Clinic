﻿using DoctorClinic.BLL;
using DoctorClinic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoctorClinic.Controllers
{
    public class DepartmentsController : Controller
    {
        bool status = false;
        DepartmentManager departmentManager;
        public DepartmentsController()
        {
            departmentManager = new DepartmentManager();

        }

        //GET: Create Department
        public ActionResult Create()
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }


        //POST: Create Department
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Department department)
        {
            if (ModelState.IsValid)
            {
                bool checkName = departmentManager.CheckDepartmentName(department.Name);
                if (checkName)
                {
                    ModelState.AddModelError("Name", "This Name is Already Exist");
                    return View();
                }
                status = departmentManager.Add(department);
                if (status)
                {

                    @ViewBag.Msg = "Department Added Successfully";
                    ModelState.Clear();

                }
                else
                {
                    @ViewBag.Msg = "Department Added Failed !";
                }
            }
            return View();
        }


        //GET: Update Department
        public ActionResult Update(int? id)
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }

            Department department = departmentManager.GetById(id);
            if (department == null)
            {
                return RedirectToAction("Error");
            }
            return View(department);
        }


        //POST: Update Department
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(Department department)
        {
            status = departmentManager.Update(department);
            if (status)
            {
                return RedirectToAction("ShowAll");
            }
            return View(department);
        }

        //Show All Department
        public ActionResult ShowAll()
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            return View(departmentManager.GetAll());
        }


        //Delete a Department
        public JsonResult Delete(int? id)
        {
            status = departmentManager.Delete(id);
            if (status)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }

        //Check Department Name Availability
        public JsonResult CheckDepartmentName(string name)
        {
            status = departmentManager.CheckDepartmentName(name);
            if (status)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }

        }
    }
}