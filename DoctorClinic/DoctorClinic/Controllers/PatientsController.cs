﻿using AppointmentClinic.BLL;
using DoctorClinic.BLL;
using DoctorClinic.Models;
using DoctorClinic.Models.Region;
using DoctorClinic.Models.ViewModel;
using PatientClinic.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PatientClinic.Controllers
{
    public class PatientsController : Controller
    {
        bool status = false;
        CommonManager commonManager = new CommonManager();
        PatientManager patientManager;
        public PatientsController()
        {
            patientManager = new PatientManager();
        }

        //GET: Create Patient
        public ActionResult Create()
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }


        //POST: Create Patient
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Patient patient, HttpPostedFileBase ImageFile)
        {

            if (ImageFile == null)
            {
                ModelState.AddModelError("Image", "Please Upload an Image");

            }

            if (ImageFile != null)
            {
                bool isValidFormat = commonManager.CheckImageFormat(ImageFile);
                if (isValidFormat == false)
                {
                    ModelState.AddModelError("Image", "Only jpg , png , jpeg are allowed");
                }
                else
                {
                    byte[] convertedImage = commonManager.ConvertImage(ImageFile);
                    patient.Image = convertedImage;
                }


            }

            if (ModelState.IsValid)
            {
                bool isExistEmail = patientManager.CheckPatientEmail(patient.Email);
                if (isExistEmail)
                {
                    ModelState.AddModelError("Name", "This Name is Already Exist");
                    return View();
                }
                status = patientManager.Add(patient);
                if (status)
                {
                    
                    @ViewBag.Msg = "Patient Added Successfully";
                    ModelState.Clear();

                }
                else
                {
                    @ViewBag.Msg = "Patient Added Failed !";
                }
            }
            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            ViewBag.DivisionId = commonManager.GetAllDivision();
          
            return View();
        }


        //GET: Update Patient
        public ActionResult Update(int? id)
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }

            Patient patient = patientManager.GetById(id);
            if (patient == null)
            {
                return RedirectToAction("Error");
            }
            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            ViewBag.DivisionId = commonManager.GetAllDivision();
            return View(patient);
        }


        //POST: Update Patient
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(Patient patient, HttpPostedFileBase ImageFile, byte[] CurrentImage,bool? IsFromProfile)
        {
            if (ImageFile == null)
            {

                patient.Image = CurrentImage;
            }

            if (ImageFile != null)
            {
                bool isValidFormat = commonManager.CheckImageFormat(ImageFile);
                if (isValidFormat == false)
                {
                    patient.Image = CurrentImage;
                    ModelState.AddModelError("Image", "Only jpg , png , jpeg are allowed");
                }
                else
                {
                    patient.Image = commonManager.ConvertImage(ImageFile);
                }

            }


            if (ModelState.IsValid)
            {

                status = patientManager.Update(patient);
                if (status == true)
                {
                    if (IsFromProfile==true)
                    {
                        return RedirectToAction("ProfileDetail", new { patient.Id });
                    }
                    return RedirectToAction("ShowAll");
                }
                if (status == false)
                {

                    ViewBag.Msg = "Party Category Added Failed !";
                }

            }
            if (IsFromProfile==true)
            {
                return RedirectToAction("ProfileDetail", new { patient.Id });
            }
            return View(patient);
        }

        //Show All Patient
        public ActionResult ShowAll()
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            return View(patientManager.GetAll());
        }


        //Detail of a Patient
        public ActionResult Details(int? id)
        {
            if ((string)Session["Role"] != "Admin")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Patient patient = patientManager.GetById(id);
            if (patient == null)
            {
                return RedirectToAction("Error", "Home");
            }


            return View(patient);
        }

        //Delete a Patient
        public JsonResult Delete(int? id)
        {
            status = patientManager.Delete(id);
            if (status)
            {
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }

        //Check Patient Email Availability
        public JsonResult CheckPatientEmail(string Email, string CurrentEmail)
        {

            if (Email == CurrentEmail)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            status = patientManager.CheckPatientEmail(Email);
            if (status)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

        }



        //Profile Information

        public ActionResult ProfileDetail(int? id)
        {
            if ((string)Session["Role"] != "Patient")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Patient patient = patientManager.GetById(id);
            if (patient == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View(patient);
        }


        //Edit Patient Profile
        public ActionResult EditProfile(int? id)
        {
            if ((string)Session["Role"] != "Patient")
            {
                return RedirectToAction("Index", "Home");
            }

            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Patient patient = patientManager.GetById(id);
            if (patient == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View(patient);
        }



        //Patients Appointments
        public ActionResult Appointments()
        {
            if ((string)Session["Role"] != "Patient")
            {
                return RedirectToAction("Index", "Home");
            }
            string email = (string)Session["PatientEmail"];
            List<Appointment> appointments =patientManager.GetAppointmentByPatientMail(email);
                
            return View(appointments);
        }


        //Details of Patients Appointment
        public ActionResult AppointmentDetails(int? id)
        {
            if ((string)Session["Role"] != "Patient")
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            AppointmentManager appointmentManager = new AppointmentManager();
            Appointment appointment = appointmentManager.GetById(id);
            if (appointment == null)
            {
                return RedirectToAction("Error", "Home");
            }

            Patient patient = appointmentManager.GetPatientByEmail(appointment.PatientEmail);
            Scheduler schedule = appointmentManager.GetSchedule(appointment.Date, appointment.DoctorId);
            //Patient patient = db.Patients.SingleOrDefault(model => model.Email == appointment.PatientId);
            //Scheduler schedule = db.Schedulers.SingleOrDefault(model => model.AvailableDay == appointment.AppoinmentDate.DayOfWeek.ToString() && model.DoctorId == appointment.DoctorId);

            AppointmentDetailVm appoinDetail = new AppointmentDetailVm();

            appoinDetail.AppointmentId = appointment.Id;
            appoinDetail.DoctorName = appointment.Doctor.FirstName + " " + appointment.Doctor.LastName;
            appoinDetail.Department = appointment.Department.Name;
            appoinDetail.Day = appointment.Date.DayOfWeek.ToString();
            appoinDetail.ScheduleTime = schedule.StartTime + "-" + schedule.EndTime;
            appoinDetail.AppointmentDate = appointment.Date;
            appoinDetail.Image = patient.Image;
            appoinDetail.FullName = patient.FirstName + " " + patient.LastName;
            appoinDetail.DateOfBirth = patient.DateOfBirth.ToString();
            appoinDetail.Age = Convert.ToString((DateTime.Now.Year - Convert.ToDateTime(patient.DateOfBirth).Year));
            appoinDetail.Gender = patient.Gender;
            appoinDetail.Mobile = patient.Mobile;
            appoinDetail.Problem = appointment.Problem;
            appoinDetail.SerialNo = "#0" + appointment.SerialNo.ToString();
            return View(appoinDetail);
        }


        //Get An Appointment
        public ActionResult GetAppointment()
        {
            if ((string)Session["Role"] != "Patient")
            {
                return RedirectToAction("Login", "Authorizations");
            }
            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            return View();
        }


        //Patient SignUp
        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp(Patient patient, HttpPostedFileBase ImageFile)
        {

            if (ImageFile == null)
            {
                ModelState.AddModelError("Image", "Please Upload an Image");

            }

            if (ImageFile != null)
            {
                bool isValidFormat = commonManager.CheckImageFormat(ImageFile);
                if (isValidFormat == false)
                {
                    ModelState.AddModelError("Image", "Only jpg , png , jpeg are allowed");
                }
                else
                {
                    byte[] convertedImage = commonManager.ConvertImage(ImageFile);
                    patient.Image = convertedImage;
                }


            }

            if (ModelState.IsValid)
            {
                bool isExistEmail = patientManager.CheckPatientEmail(patient.Email);
                if (isExistEmail)
                {
                    ModelState.AddModelError("Name", "This Name is Already Exist");
                    return View();
                }
                status = patientManager.Add(patient);
                if (status)
                {

                    @ViewBag.Msg = "Patient Added Successfully";
                    ModelState.Clear();
                    return RedirectToAction("Login", "Authorizations");

                }
                else
                {
                    @ViewBag.Msg = "Patient Added Failed !";
                }
            }
            ViewBag.DepartmentId = commonManager.GetAllDepartment();
            ViewBag.DivisionId = commonManager.GetAllDivision();

            return View();
        }
    }
}