﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctorClinic.Models.Region
{
    public class SubDistrict
    {
        public int Id { get; set; }
        public string Name  { get; set; }
        public int DistrictId { get; set; }
        public virtual District District { get; set; }
        public virtual List<Hospital> Hospitals { get; set; }
        public virtual List<Doctor> Doctors { get; set; }
    }
}