﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoctorClinic.Models.Region
{
    public class Division
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public virtual List<District> Districts { get; set; }
        public virtual List<Doctor> Doctors { get; set; }
        public virtual List<Hospital> Hospitals { get; set; }
    }
}