﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctorClinic.Models.Region
{
    public class District
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int DivisionId { get; set; }
        public virtual Division Division { get; set; }
        public virtual List<SubDistrict> SubDistricts { get; set; }
        public virtual List<Doctor> Doctors { get; set; }
        public virtual List<Hospital> Hospitals { get; set; }

    }
}