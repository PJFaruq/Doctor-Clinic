﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorClinic.Models.Contracts
{
    public interface ICommonModel
    {
        bool IsDeleted { get; set; }
    }
}
