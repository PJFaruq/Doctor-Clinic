﻿using DoctorClinic.Models.Region;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoctorClinic.Models
{
    public class Hospital
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Please Enter Name")]
        [StringLength(100, ErrorMessage = "The Name is in Between 2 to 100 Character",MinimumLength =2)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please Enter Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please Enter Contact Number")]
        public string ContactNo { get; set; }

        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public bool IsDeleted { get; set; }

        [Display(Name="Division")]
        [Required(ErrorMessage = "Please Select Division")]
        public int DivisionId { get; set; }
        public virtual Division Division { get; set; }

        [Display(Name = "District")]
        [Required(ErrorMessage = "Please Select District")]
        public int  DistrictId { get; set; }
        public virtual District District { get; set; }

        [Display(Name = "Sub District")]
        [Required(ErrorMessage = "Please Select Sub District")]
        public int SubDistrictId { get; set; }
        public virtual SubDistrict SubDistrict { get; set; }

        public virtual List<Doctor> Doctors { get; set; }
    }
}