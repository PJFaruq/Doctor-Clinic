﻿using DoctorClinic.Models.Region;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoctorClinic.Models
{
    public class Doctor
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(20, ErrorMessage = "Maximum 20 Characters are Allowed")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(20, ErrorMessage = "Maximum 20 Characters are Allowed")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [Remote("CheckDoctorEmail", "Doctors", ErrorMessage = "This Email is Already Exist", AdditionalFields = "CurrentEmail")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please Enter a Valid Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Maximum 50 Characters are Allowed")]
        public string Designation { get; set; }

        [Required]
        [Display(Name = "Department")]
        public int DepartmentId { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [StringLength(200, ErrorMessage = "Maximum 200 Characters are Allowed")]
        public string Address { get; set; }

        public string Telephone { get; set; }

        [Required]
        [Display(Name = "Mobile")]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }


        [Display(Name = "Short Biography")]
        [StringLength(1000, ErrorMessage = "Maximum 1000 Character are Allowed")]
        [DataType(DataType.MultilineText)]
        public string ShortBio { get; set; }

        [Display(Name = "Photo")]
        public byte[] Image { get; set; }

        [Display(Name = "Speciality")]
        [DataType(DataType.MultilineText)]
        [StringLength(500, ErrorMessage = "Maximum 500 Character are Allowed")]
        public string Specialist { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Required]
        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }

        [Display(Name = "Education")]
        [DataType(DataType.MultilineText)]
        [StringLength(2000, ErrorMessage = "Maximum 2000 Character are Allowed")]
        public string Education { get; set; }

        public bool IsDeleted { get; set; }
        public virtual Department Department { get; set; }

        [Display(Name = "Division")]
        [Required(ErrorMessage = "Please Select Division")]
        public int DivisionId { get; set; }
        public virtual Division Division { get; set; }

        [Display(Name = "District")]
        [Required(ErrorMessage = "Please Select District")]
        public int DistrictId { get; set; }
        public virtual District District { get; set; }

        [Display(Name = "Sub District")]
        [Required(ErrorMessage = "Please Select Sub District")]
        public int SubDistrictId { get; set; }
        public virtual SubDistrict SubDistrict { get; set; }

        [Display(Name = "Hospital")]
        [Required(ErrorMessage = "Please Select Hospital")]
        public int HospitalId { get; set; }
        public virtual Hospital Hospital { get; set; }
        public virtual List<Appointment> Appointments { get; set; }
    }
}