﻿using DoctorClinic.Models.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoctorClinic.Models
{
    public class Appointment: ICommonModel
    {
        public int Id { get; set; }

        [Display(Name = "Patient Id/Email")]
        [Required(ErrorMessage = "Please Enter Patient Id/Email")]
        //[Remote("CheckPatient", "Appoinments", ErrorMessage = "This Patient is Not Exist")]
        //[Remote("CheckPatient", "Appoinments", ErrorMessage = "This Patient is Not Exist")]
        public string PatientEmail { get; set; }

        [Required(ErrorMessage = "Please Select a Department")]
        [Display(Name = "Department")]
        public int DepartmentId { get; set; }

        [Required(ErrorMessage = "Please Select a Doctor")]
        [Display(Name = "Doctor Name")]
        public int DoctorId { get; set; }

        [Required(ErrorMessage = "Please Select a Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [Display(Name = "Appointment Date")]
        public System.DateTime Date { get; set; }

        [Required(ErrorMessage = "Please Select Serial No.")]
        [Display(Name = "Serial No.")]
        public int SerialNo { get; set; }

        [Display(Name = "Problem")]
        [DataType(DataType.MultilineText)]
        public string Problem { get; set; }
        public bool IsDeleted { get; set; }


        public virtual Department Department { get; set; }
        public virtual Doctor Doctor { get; set; }
    }
}