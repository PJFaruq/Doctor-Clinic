﻿using DoctorClinic.Models.Region;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoctorClinic.Models.DatabaseContext
{
    public class DoctorClinicDbContext: DbContext
    {
        public DbSet<Department> Departments { get; set; }
        public DbSet<Doctor> Doctors { get; set; }

        public DbSet<Division> Divisions { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<SubDistrict> SubDistricts { get; set; }
        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Scheduler> Schedulers { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<District>()
                .HasRequired(d => d.Division)
                .WithMany(w => w.Districts)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hospital>()
                .HasRequired(d => d.District)
                .WithMany(w => w.Hospitals)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hospital>()
                .HasRequired(d => d.SubDistrict)
                .WithMany(w => w.Hospitals)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hospital>()
                .HasRequired(d => d.Division)
                .WithMany(w => w.Hospitals)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubDistrict>()
                .HasRequired(d => d.District)
                .WithMany(w => w.SubDistricts)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Doctor>()
                .HasRequired(d => d.Division)
                .WithMany(w => w.Doctors)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Doctor>()
                .HasRequired(d => d.District)
                .WithMany(w => w.Doctors)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Doctor>()
                .HasRequired(d => d.SubDistrict)
                .WithMany(w => w.Doctors)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Appointment>()
                .HasRequired(d => d.Doctor)
                .WithMany(w => w.Appointments)
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }

    }
}