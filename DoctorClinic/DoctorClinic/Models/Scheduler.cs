﻿using DoctorClinic.Models.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoctorClinic.Models
{
    public class Scheduler:ICommonModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Doctor Name")]
        public int DoctorId { get; set; }

        [Required]
        [Display(Name = "Available Day")]
        [Remote("CheckDoctorIdAndDay", "Schedulers", ErrorMessage = "There is a Schedule with this Doctor and Day", AdditionalFields = "DoctorId,EditAvailableDay,EditDoctorId")]
        public string AvailableDay { get; set; }

        [Required]
        [Display(Name = "Start Time")]
        [DataType(DataType.Time)]
        public System.TimeSpan StartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [Display(Name = "End Time")]
        public System.TimeSpan EndTime { get; set; }

        [Required]
        [Display(Name ="Per Patient Duration")]
        public int PerPatientDuration { get; set; }
        public bool IsDeleted { get; set; }
        public virtual Doctor Doctor { get; set; }
    }
}