﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoctorClinic.Models
{
    public class Department
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The length should be 3 to 20 Character")]
        public string Name { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        public virtual List<Doctor> Doctors { get; set; }
        public virtual List<Appointment> Appointments { get; set; }
    }
}