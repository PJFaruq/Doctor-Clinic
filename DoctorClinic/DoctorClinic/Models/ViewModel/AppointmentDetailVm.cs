﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoctorClinic.Models.ViewModel
{
    public class AppointmentDetailVm
    {
        public int AppointmentId { get; set; }

        [Display(Name = "Doctor Name:")]
        public string DoctorName { get; set; }

        [Display(Name = "Department:")]
        public string Department { get; set; }

        [Display(Name = "Day:")]
        public string Day { get; set; }

        [Display(Name = "Schedule Time:")]
        public string ScheduleTime { get; set; }

        [Display(Name = "Serial No.")]
        public string SerialNo { get; set; }

        [Display(Name = "Appoinment Date:")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime AppointmentDate { get; set; }

        [Display(Name = "Full Name:")]
        public string FullName { get; set; }

        [Display(Name = "Date Of Birth:")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Age:")]
        public string Age { set; get; }

        [Display(Name = "Gender:")]
        public string Gender { get; set; }

        [Display(Name = "Mobile Number:")]
        public string Mobile { get; set; }

        [Display(Name = "Problem:")]
        public string Problem { get; set; }
        public byte[] Image { get; set; }
    }
}