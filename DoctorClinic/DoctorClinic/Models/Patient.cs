﻿using DoctorClinic.Models.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoctorClinic.Models
{
    public class Patient:ICommonModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(20, ErrorMessage = "Maximum 20 Characters are Allowed")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(20, ErrorMessage = "Maximum 20 Characters are Allowed")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please Enter a Valid Email")]
        [Remote("CheckPatientEmail", "Patients", ErrorMessage = "This Email is Already Exist", AdditionalFields = "CurrentEmail")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(15, ErrorMessage = "Password Length Should be between 4 to 15", MinimumLength = 4)]
        public string Password { get; set; }

        [Display(Name = "Telephone")]
        public string Telephone { get; set; }

        [Required]
        [Display(Name = "Mobile")]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }

        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }

        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Photo")]
        public byte[] Image { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(200, ErrorMessage = "Maximum 200 Characters are Allowed")]
        public string Address { get; set; }
        public bool IsDeleted { get; set; }
        
    }
}