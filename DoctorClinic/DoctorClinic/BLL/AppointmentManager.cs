﻿using DoctorClinic.BLL.Base;
using DoctorClinic.Models;
using DoctorClinic.Repository;
using DoctorClinic.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppointmentClinic.BLL
{
    public class AppointmentManager:BaseManager<Appointment>
    {
        bool status = false;
        AppointmentRepository appointmentRepository = new AppointmentRepository();
        public AppointmentManager(BaseRepository<Appointment> baseRepository) : base(baseRepository)
        {
        }

        public AppointmentManager() : base(new AppointmentRepository())
        {

        }

        internal bool CheckPatient(string patientEmail)
        {
            status = appointmentRepository.CheckPatient(patientEmail);
            return status;
        }

        internal Patient GetPatientByEmail(string patientEmail)
        {
            Patient patient =appointmentRepository.GetPatientByEmail(patientEmail);
            return patient;

        }

        internal List<Doctor> GetDoctorListByDepartmentId(int departmentId)
        {
            List<Doctor> doctorList = appointmentRepository.GetDoctorListByDepartmentId(departmentId);
            return doctorList;
        }

        internal List<int> CheckSerial(DateTime date, int doctorId)
        {
            List<int> SerialList = new List<int>();
            bool isAvailable = appointmentRepository.CheckSerial(date, doctorId);

            if (isAvailable)
            {
                //Scheduler schedule = db.Schedulers.Single(model => model.DoctorId == doctorId && model.AvailableDay == date.DayOfWeek.ToString());

                Scheduler schedule = appointmentRepository.GetScheduleForSerial(date, doctorId); 

                int serial = Convert.ToInt32((schedule.EndTime.TotalMinutes - schedule.StartTime.TotalMinutes) / schedule.PerPatientDuration);

                for (int i = 1; i <= serial; i++)
                {
                    SerialList.Add(i);
                }

                List<int> UsedSerialList = appointmentRepository.GetUsedSerialList(date, doctorId);

                List<int> newList = SerialList.Except(UsedSerialList).ToList();
                return newList;
            }
            return SerialList;
            
        }

        internal Scheduler GetSchedule(DateTime date, int doctorId)
        {
            Scheduler schedule = appointmentRepository.GetSchedule(date, doctorId);
            return schedule;
        }


    }
}