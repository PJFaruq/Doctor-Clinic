﻿using DoctorClinic.BLL.Base;
using DoctorClinic.Models;
using DoctorClinic.Repository;
using DoctorClinic.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctorClinic.BLL
{
    public class DepartmentManager : BaseManager<Department>
    {
        DepartmentRepositoy departmentRepo;
        public DepartmentManager(BaseRepository<Department> baseRepository) : base(baseRepository)
        {
        }
        public DepartmentManager():base(new DepartmentRepositoy())
        {
            departmentRepo= new DepartmentRepositoy();
        }

        public bool CheckDepartmentName(string name)
        {
            return departmentRepo.CheckDepartmentName(name);
        }
    }
}