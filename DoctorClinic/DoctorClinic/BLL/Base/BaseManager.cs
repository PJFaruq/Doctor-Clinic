﻿using DoctorClinic.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctorClinic.BLL.Base
{
    public class BaseManager<T> where T : class
    {
        BaseRepository<T> baseRepository;
        public BaseManager(BaseRepository<T> baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        public virtual bool Add(T entity)
        {
            return baseRepository.Add(entity);
        }
        public virtual bool Update(T entity)
        {
            return baseRepository.Update(entity);
        }
        public virtual bool Delete(int? id)
        {
            return baseRepository.Delete(id);
        }

        public virtual T GetById(int? id)
        {
            T entity = baseRepository.GetById(id);
            return entity;
        }

        public virtual List<T> GetAll()
        {

            return baseRepository.GetAll();
        }
    }
}