﻿using DoctorClinic.BLL.Base;
using DoctorClinic.Models;
using DoctorClinic.Repository;
using DoctorClinic.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientClinic.BLL
{
    public class PatientManager:BaseManager<Patient>
    {
        PatientRepository patientRepository = new PatientRepository();
        public PatientManager(BaseRepository<Patient> baseRepository) : base(baseRepository)
        {
        }

        public PatientManager() : base(new PatientRepository())
        {

        }

        internal bool CheckPatientEmail(string email)
        {
            return patientRepository.CheckPatientEmail(email);
        }

        internal List<Appointment> GetAppointmentByPatientMail(string email)
        {
            List<Appointment> appointments = patientRepository.GetAppointmentByPatientMail(email);
            return appointments;
        }
    }
}