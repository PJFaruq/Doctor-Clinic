﻿using DoctorClinic.Models;
using DoctorClinic.Models.Region;
using DoctorClinic.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoctorClinic.BLL
{
    public class CommonManager
    {
        CommonRepository commonRepo = new CommonRepository();
        bool status = false;
        public SelectList GetAllDepartment()
        {
            return commonRepo.GetAllDepartment();
        }

        internal SelectList GetAllDivision()
        {
            return commonRepo.GetAllDivision();
        }

        internal List<SubDistrict> GetAllSubDistrictById(int id)
        {
            return commonRepo.GetAllSubDistrictById(id);
        }

        internal List<District> GetAllDistrictById(int id)
        {
            return commonRepo.GetAllDistrictById( id);
        }

        internal List<Hospital> GetAllHospitalById(int id)
        {
            return commonRepo.GetAllHospitalById( id);
        }

        internal bool CheckImageFormat(HttpPostedFileBase imageFile)
        {
            if (imageFile != null)
            {
                var extension = Path.GetExtension(imageFile.FileName).ToLower();
                var fileName = Path.GetFileName(imageFile.FileName);

                var allowExtension = new[]
                {
                         ".jpg",".png",".jpeg"
                    };
                if (allowExtension.Contains(extension))
                {
                    status = true;
                }
            }
            return status;
        }

        internal byte[] ConvertImage(HttpPostedFileBase imageFile)
        {
            byte[] convertedImage = new byte[imageFile.ContentLength];
            imageFile.InputStream.Read(convertedImage, 0, imageFile.ContentLength);
            return convertedImage;
        }

        internal SelectList GetSelectListOfDoctorById(int ? DoctorId)
        {
            return commonRepo.GetSelectListOfDoctorById(DoctorId);
        }

        public SelectList GetSelectListOfDoctor()
        {
            return commonRepo.GetSelectListOfDoctor();
        }
    }
}