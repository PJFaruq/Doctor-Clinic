﻿using DoctorClinic.Models;
using DoctorClinic.Repository;
using DoctorClinic.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DoctorClinic.BLL.Base;

namespace DoctorClinic.BLL
{
    public class SchedulerManager:BaseManager<Scheduler>
    {
        SchedulerRepository schedulerRepository = new SchedulerRepository();
        public SchedulerManager(BaseRepository<Scheduler> baseRepository) : base(baseRepository)
        {
        }

        public SchedulerManager() : base(new SchedulerRepository())
        {

        }

        internal bool CheckExistingSchedule(string availableDay, int doctorId)
        {
            bool IsAvailable = schedulerRepository.CheckExistingSchedule(availableDay, doctorId);
            return IsAvailable;
        }
    }
}