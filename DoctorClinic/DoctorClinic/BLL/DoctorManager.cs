﻿using DoctorClinic.BLL.Base;
using DoctorClinic.Models;
using DoctorClinic.Repository;
using DoctorClinic.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctorClinic.BLL
{
    public class DoctorManager : BaseManager<Doctor>
    {
        DoctorRepository doctorRepository = new DoctorRepository();
        public DoctorManager(BaseRepository<Doctor> baseRepository) : base(baseRepository)
        {
        }

        public DoctorManager():base(new DoctorRepository())
        {

        }

        internal bool CheckDoctorEmail(string email)
        {
            return doctorRepository.CheckDoctorEmail(email);
        }

        internal List<Appointment> GetAppointmentByDoctorId(int id)
        {
            List<Appointment> appointments = doctorRepository.GetAppointmentByDoctorId(id);
            return appointments;
        }
    }
}