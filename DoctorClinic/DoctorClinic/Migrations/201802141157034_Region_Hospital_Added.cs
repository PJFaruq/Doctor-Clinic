namespace DoctorClinic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Region_Hospital_Added : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Districts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DivisionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Divisions", t => t.DivisionId)
                .Index(t => t.DivisionId);
            
            CreateTable(
                "dbo.Divisions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Hospitals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false),
                        ContactNo = c.String(nullable: false),
                        Address = c.String(),
                        Description = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DivisionId = c.Int(nullable: false),
                        DistrictId = c.Int(nullable: false),
                        SubDistrictId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Districts", t => t.DistrictId)
                .ForeignKey("dbo.Divisions", t => t.DivisionId)
                .ForeignKey("dbo.SubDistricts", t => t.SubDistrictId)
                .Index(t => t.DivisionId)
                .Index(t => t.DistrictId)
                .Index(t => t.SubDistrictId);
            
            CreateTable(
                "dbo.SubDistricts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DistrictId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Districts", t => t.DistrictId)
                .Index(t => t.DistrictId);
            
            AddColumn("dbo.Doctors", "DivisionId", c => c.Int(nullable: false));
            AddColumn("dbo.Doctors", "DistrictId", c => c.Int(nullable: false));
            AddColumn("dbo.Doctors", "SubDistrictId", c => c.Int(nullable: false));
            AddColumn("dbo.Doctors", "HospitalId", c => c.Int(nullable: false));
            CreateIndex("dbo.Doctors", "DivisionId");
            CreateIndex("dbo.Doctors", "DistrictId");
            CreateIndex("dbo.Doctors", "SubDistrictId");
            CreateIndex("dbo.Doctors", "HospitalId");
            AddForeignKey("dbo.Doctors", "HospitalId", "dbo.Hospitals", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Doctors", "DistrictId", "dbo.Districts", "Id");
            AddForeignKey("dbo.Doctors", "DivisionId", "dbo.Divisions", "Id");
            AddForeignKey("dbo.Doctors", "SubDistrictId", "dbo.SubDistricts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Doctors", "SubDistrictId", "dbo.SubDistricts");
            DropForeignKey("dbo.Doctors", "DivisionId", "dbo.Divisions");
            DropForeignKey("dbo.Doctors", "DistrictId", "dbo.Districts");
            DropForeignKey("dbo.Districts", "DivisionId", "dbo.Divisions");
            DropForeignKey("dbo.Hospitals", "SubDistrictId", "dbo.SubDistricts");
            DropForeignKey("dbo.SubDistricts", "DistrictId", "dbo.Districts");
            DropForeignKey("dbo.Doctors", "HospitalId", "dbo.Hospitals");
            DropForeignKey("dbo.Hospitals", "DivisionId", "dbo.Divisions");
            DropForeignKey("dbo.Hospitals", "DistrictId", "dbo.Districts");
            DropIndex("dbo.SubDistricts", new[] { "DistrictId" });
            DropIndex("dbo.Hospitals", new[] { "SubDistrictId" });
            DropIndex("dbo.Hospitals", new[] { "DistrictId" });
            DropIndex("dbo.Hospitals", new[] { "DivisionId" });
            DropIndex("dbo.Districts", new[] { "DivisionId" });
            DropIndex("dbo.Doctors", new[] { "HospitalId" });
            DropIndex("dbo.Doctors", new[] { "SubDistrictId" });
            DropIndex("dbo.Doctors", new[] { "DistrictId" });
            DropIndex("dbo.Doctors", new[] { "DivisionId" });
            DropColumn("dbo.Doctors", "HospitalId");
            DropColumn("dbo.Doctors", "SubDistrictId");
            DropColumn("dbo.Doctors", "DistrictId");
            DropColumn("dbo.Doctors", "DivisionId");
            DropTable("dbo.SubDistricts");
            DropTable("dbo.Hospitals");
            DropTable("dbo.Divisions");
            DropTable("dbo.Districts");
        }
    }
}
