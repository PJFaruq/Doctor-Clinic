namespace DoctorClinic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SchedulePerpatientDurationAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schedulers", "PerPatientDuration", c => c.Int(nullable: false));
            DropColumn("dbo.Schedulers", "PerPatientTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Schedulers", "PerPatientTime", c => c.Time(nullable: false, precision: 7));
            DropColumn("dbo.Schedulers", "PerPatientDuration");
        }
    }
}
