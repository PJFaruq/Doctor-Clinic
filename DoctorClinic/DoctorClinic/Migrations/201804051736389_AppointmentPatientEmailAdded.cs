namespace DoctorClinic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AppointmentPatientEmailAdded : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Appointments", "PatientId", "dbo.Patients");
            DropIndex("dbo.Appointments", new[] { "PatientId" });
            AddColumn("dbo.Appointments", "PatientEmail", c => c.String(nullable: false));
            DropColumn("dbo.Appointments", "PatientId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Appointments", "PatientId", c => c.Int(nullable: false));
            DropColumn("dbo.Appointments", "PatientEmail");
            CreateIndex("dbo.Appointments", "PatientId");
            AddForeignKey("dbo.Appointments", "PatientId", "dbo.Patients", "Id", cascadeDelete: true);
        }
    }
}
