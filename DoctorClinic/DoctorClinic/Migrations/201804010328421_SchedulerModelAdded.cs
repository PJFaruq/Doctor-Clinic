namespace DoctorClinic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SchedulerModelAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Schedulers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DoctorId = c.Int(nullable: false),
                        AvailableDay = c.String(nullable: false),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                        PerPatientTime = c.Time(nullable: false, precision: 7),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Doctors", t => t.DoctorId, cascadeDelete: true)
                .Index(t => t.DoctorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Schedulers", "DoctorId", "dbo.Doctors");
            DropIndex("dbo.Schedulers", new[] { "DoctorId" });
            DropTable("dbo.Schedulers");
        }
    }
}
