namespace DoctorClinic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Nullable_Image_Change_in_Doctor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Doctors", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Doctors", "Image");
        }
    }
}
