namespace DoctorClinic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PatientSexChangetoGender : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Patients", "Gender", c => c.String());
            DropColumn("dbo.Patients", "Sex");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patients", "Sex", c => c.String());
            DropColumn("dbo.Patients", "Gender");
        }
    }
}
