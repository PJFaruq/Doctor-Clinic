﻿using DoctorClinic.Models;
using DoctorClinic.Models.DatabaseContext;
using DoctorClinic.Repository.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoctorClinic.Repository
{
    public class AppointmentRepository:BaseRepository<Appointment>
    {
        private DoctorClinicDbContext Context
        {
            get
            {
                return Db as DoctorClinicDbContext;
            }
        }
        public AppointmentRepository(DbContext db) : base(db)
        {
        }
        public AppointmentRepository() : base(new DoctorClinicDbContext())
        {

        }

        internal bool CheckPatient(string patientEmail)
        {
            return Context.Patients.Any(model => model.Email == patientEmail);
        }

        internal Patient GetPatientByEmail(string patientEmail)
        {
           return Context.Patients.Single(model => model.Email == patientEmail);
        }

        internal List<Doctor> GetDoctorListByDepartmentId(int departmentId)
        {
            Context.Configuration.ProxyCreationEnabled = false;
            return Context.Doctors.Where(model => model.DepartmentId == departmentId).ToList();
        }

        internal bool CheckSerial(DateTime date, int doctorId)
        {
            return Context.Schedulers.Any(model => model.DoctorId == doctorId && model.AvailableDay == date.DayOfWeek.ToString());
        }

        internal Scheduler GetScheduleForSerial(DateTime date, int doctorId)
        {
          return  (from model in Context.Schedulers where model.DoctorId == doctorId && model.AvailableDay == date.DayOfWeek.ToString() select model).First();
        }

        internal List<int> GetUsedSerialList(DateTime date, int doctorId)
        {
          return  Context.Appointments.Where(model => model.DoctorId == doctorId && model.Date == date).Select(model => model.SerialNo).ToList();
        }

        internal Scheduler GetSchedule(DateTime date, int doctorId)
        {
            return Context.Schedulers.SingleOrDefault(model => model.AvailableDay == date.DayOfWeek.ToString() && model.DoctorId == doctorId);
        }
    }
}