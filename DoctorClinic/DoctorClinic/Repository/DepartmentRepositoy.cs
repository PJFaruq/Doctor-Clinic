﻿using DoctorClinic.Models;
using DoctorClinic.Models.DatabaseContext;
using DoctorClinic.Repository.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoctorClinic.Repository
{
    public class DepartmentRepositoy : BaseRepository<Department>
    {
        
        public DoctorClinicDbContext Context
        {
            get
            {
                return Db as DoctorClinicDbContext;
            }
            
        }
        public DepartmentRepositoy(DbContext db) : base(db)
        {
        }

        public DepartmentRepositoy():base(new DoctorClinicDbContext())
        {

        }

        public bool CheckDepartmentName(string name)
        {
            return false;

        }
    }
}