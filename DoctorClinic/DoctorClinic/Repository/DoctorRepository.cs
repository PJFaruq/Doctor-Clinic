﻿using DoctorClinic.Models;
using DoctorClinic.Models.DatabaseContext;
using DoctorClinic.Repository.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoctorClinic.Repository
{
    public class DoctorRepository : BaseRepository<Doctor>
    {
        private DoctorClinicDbContext Context
        {
            get
            {
                return Db as DoctorClinicDbContext;
            }
        }
        public DoctorRepository(DbContext db) : base(db)
        {
        }
        public DoctorRepository():base(new DoctorClinicDbContext())
        {

        }

        internal bool CheckDoctorEmail(string email)
        {
            return Context.Doctors.Any(m => m.Email == email);
        }

        internal List<Appointment> GetAppointmentByDoctorId(int id)
        {
            List<Appointment> appointments = Context.Appointments.Where(m => m.DoctorId == id).ToList();
            return appointments;
        }
    }
}