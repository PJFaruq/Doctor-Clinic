﻿using DoctorClinic.Models;
using DoctorClinic.Models.DatabaseContext;
using DoctorClinic.Models.Region;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoctorClinic.Repository
{
    public class CommonRepository
    {
        private DoctorClinicDbContext db = new DoctorClinicDbContext();
        public SelectList GetAllDepartment()
        {
            return new SelectList(db.Departments, "Id", "Name");
        }

        internal SelectList GetAllDivision()
        {
            return new SelectList(db.Divisions, "Id", "Name");
        }

        internal List<SubDistrict> GetAllSubDistrictById(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.SubDistricts.Where(m => m.DistrictId == id).ToList();
        }

        internal List<District> GetAllDistrictById(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return db.Districts.Where(m => m.DivisionId == id).ToList();
        }

        internal List<Hospital> GetAllHospitalById(int id)
        {
            return db.Hospitals.Where(m => m.SubDistrictId == id).ToList();
        }


        public SelectList GetSelectListOfDoctor()
        {
            return new SelectList(from x in db.Doctors where x.IsDeleted == false select new { x.Id, x.FirstName, x.LastName, FullName = x.FirstName + " " + x.LastName }, "Id", "FullName");
        }

        internal SelectList GetSelectListOfDoctorById(int? DoctorId)
        {
           return new SelectList(
from x in db.Doctors where x.IsDeleted == false select new { x.Id, x.FirstName, x.LastName, FullName = x.FirstName + " " + x.LastName }, "Id", "FullName", DoctorId);

        }
    }
}