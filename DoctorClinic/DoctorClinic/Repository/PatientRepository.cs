﻿using DoctorClinic.Models;
using DoctorClinic.Models.DatabaseContext;
using DoctorClinic.Repository.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoctorClinic.Repository
{
    public class PatientRepository:BaseRepository<Patient>
    {
        private DoctorClinicDbContext Context
        {
            get
            {
                return Db as DoctorClinicDbContext;
            }
        }
        public PatientRepository(DbContext db) : base(db)
        {
        }
        public PatientRepository() : base(new DoctorClinicDbContext())
        {

        }

        internal bool CheckPatientEmail(string email)
        {
            return Context.Patients.Any(m => m.Email == email);
        }

        internal List<Appointment> GetAppointmentByPatientMail(string email)
        {
            return (from model in Context.Appointments where model.PatientEmail == email select model).ToList();
        }
    }
}