﻿using DoctorClinic.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoctorClinic.Repository.Base
{
    public abstract class BaseRepository<T> where T : class
    {
        public DbContext Db { get; private set; }
        public BaseRepository(DbContext db)
        {
            Db = db;
        }

        public DbSet<T> Table
        {
            get
            {
                return Db.Set<T>();
            }
        }

        public virtual bool Add(T entity)
        {
            Table.Add(entity);
            return Db.SaveChanges() > 0;
        }

        public virtual bool Update(T entity)
        {
            Table.Attach(entity);
            Db.Entry(entity).State = EntityState.Modified;
            return Db.SaveChanges() > 0;
        }

        public virtual bool Delete(int? id)
        {
            T entity = Table.Find(id);
            Table.Remove(entity);
            return Db.SaveChanges() > 0;
        }

        public virtual T GetById(int? id)
        {
            T entity = Table.Find(id);
            return entity;
        }

        public virtual List<T> GetAll()
        {
            return Table.ToList();
        }
    }
}