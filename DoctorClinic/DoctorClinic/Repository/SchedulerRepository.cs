﻿using DoctorClinic.Models.DatabaseContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using DoctorClinic.Models;
using DoctorClinic.Repository.Base;

namespace DoctorClinic.Repository
{
    public class SchedulerRepository:BaseRepository<Scheduler>
    {
        private DoctorClinicDbContext Context
        {
            get
            {
                return Db as DoctorClinicDbContext;
            }
        }
        public SchedulerRepository(DbContext db) : base(db)
        {
        }
        public SchedulerRepository() : base(new DoctorClinicDbContext())
        {

        }

        internal bool CheckPatientEmail(string email)
        {
            return Context.Patients.Any(m => m.Email == email);
        }

        internal bool CheckExistingSchedule(string availableDay, int doctorId)
        {
           bool isAvailable= Context.Schedulers.Any(model => model.AvailableDay == availableDay && model.DoctorId == doctorId);
            return isAvailable;
        }
    }
}